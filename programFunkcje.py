import numpy as np
from matplotlib.patches import Rectangle


class SeqData:
    def __init__(self, description, seq):
        self.description = description
        self.seq = seq


# podaj gi or accesion
def getSeqDataFromNcbi(gi):
    from Bio import Entrez
    from Bio import SeqIO
    Entrez.email = "A.N.Other@example.com"
    with Entrez.efetch(
            db="nuccore", rettype="fasta", retmode="text", id=gi
    ) as handle:
        seq_record = SeqIO.read(handle, "fasta")
    seqData = SeqData(seq_record.description, seq_record.seq)
    return seqData


# 1393630358
# 1400687864
# print(getSeqDataFromNcbi("1393630358","nuccore").description)

# z konsoli
def getSeqDataFromInput(description, seq):
    return SeqData(description, seq)


# z pliku
def getSeqDataFromFile(filename):
    with open(filename, "r") as myfile:
        Lines = myfile.readlines()
        line = Lines[0]
    if line.startswith('>'):
        seqData = SeqData("", "")
        seqData.description = line[1:len(line)]
        seqData.seq = "".join(Lines[1:len(Lines)]).strip()
    return seqData


def split(word):
    return [char for char in word]


def submatrix(filename):
    s1 = []
    s2 = []
    with open(filename, 'r') as f:
        first_line = f.readline().rstrip().upper()
        chars = first_line.split(" ")
        s1 = chars[1:len(chars)]
        subarray = np.empty((0, len(s1)), int)
        for line in f:
            line = line.rstrip().upper()
            chars = line.split(" ")
            s2.append(chars[0])
            subarray = np.vstack([subarray, chars[1:len(chars)]])
    f.close()
    return s1, s2, subarray


def getScore(char1, char2, s1, s2, subarray):
    matchScore = 0
    ismatch = 0
    index1 = s1.index(char1.upper())
    index2 = s2.index(char2.upper())
    if char1 == char2:
        ismatch = 1
    matchScore = int(subarray[index1, index2])
    return matchScore, ismatch


def wypelnijTablicePunktow(gap, seq1, seq2, s1, s2, subarray):
    arr = np.zeros([len(seq1) + 1, len(seq2) + 1])
    ar2 = arr[0:len(seq1) + 1, 0]
    for index, x in np.ndenumerate(ar2):
        arr[index[0], 0] = 0
    ar2 = arr[0, 0:len(seq2) + 1]
    for index, x in np.ndenumerate(ar2):
        arr[0, index[0]] = 0
    for index, x in np.ndenumerate(arr[1:arr.shape[0], 1:arr.shape[1]]):
        matchScore, ismatch = arr[index[0], index[1]] + getScore(seq1[index[0]], seq2[index[1]], s1, s2, subarray)
        gapIns = gap + arr[index[0], index[1] + 1]
        gapDel = gap + arr[index[0] + 1, index[1]]
        arr[index[0] + 1, index[1] + 1] = max(matchScore, gapIns, gapDel, 0)
    return arr


def getSciezkaDopasowania(seq1, seq2, F, gap, s1, s2, subarray):
    aln1 = ""
    aln2 = ""
    i = len(seq1)
    j = len(seq2)
    seq1 = "-" + seq1
    seq2 = "-" + seq2
    score = 0
    sciezka = np.zeros([len(seq1), len(seq2)])
    maximum = np.amax(F)
    start = np.argwhere(F == maximum)
    start = start[0]
    i = start[0]
    j = start[1]
    ends1 = i
    ends2 = j
    starts1 = 0
    starts2 = 0

    while (i > 0 or j > 0) and F[i, j] != 0:
        starts1 = i
        starts2 = j
        matchScore, ismatch = getScore(seq1[i], seq2[j], s1, s2, subarray)
        if i > 0 and j > 0 and F[i, j] == F[i - 1, j - 1] + matchScore:
            aln1 = seq1[i] + aln1
            aln2 = seq2[j] + aln2
            sciezka[i, j] = 1
            i = i - 1
            j = j - 1
            score = score + matchScore
        elif i > 0 and F[i, j] == F[i - 1, j] + gap:
            aln1 = seq1[i] + aln1
            aln2 = "-" + aln2
            sciezka[i, j] = 1
            i = i - 1
            score = score + gap
        else:
            aln1 = "-" + aln1
            aln2 = seq2[j] + aln2
            sciezka[i, j] = 1
            j = j - 1
            score = score + gap
        sciezka[i, j] = 1


    return sciezka, score, aln1, aln2, starts1, ends1, starts2, ends2


def run():
    sposob = input("Podaj sposób wczytywania: 1 ręcznie, 2 z pliku, 3 z bazy ncbi\n")
    if sposob == "1":
        print("Podaj nazwę pierwszej sekwencji:\n")
        print("Podaj pierwszą sekwencje:\n")
        seq1Data = getSeqDataFromInput(input(), input())
        print("Podaj nazwę drugiej sekwencji:\n")
        print("Podaj drugą sekwencje:\n")
        seq2Data = getSeqDataFromInput(input(), input())

    elif sposob == "2":
        seq1Data = getSeqDataFromFile(input("Podaj nazwę pliku pierwszej sekwencji\n"))
        seq2Data = getSeqDataFromFile(input("Podaj nazwę pliku drugiej sekwencji\n"))
    elif sposob == "3":
        seq1Data = getSeqDataFromNcbi(input("Podaj GI lub ACCESION pierwszej sekwencji\n"))
        seq2Data = getSeqDataFromNcbi(input("Podaj GI lub ACCESION drugiej sekwencji\n"))
    filename = (input("Podaj nazwę pliku z macierzą podobieństwa:\n"))
    gap = int(input("Punkty za gap:\n"))

    if len(seq1Data.seq) * len(seq2Data.seq) > 10000:
        seq1Data.seq = seq1Data.seq[0:99]
        seq2Data.seq = seq2Data.seq[0:99]
    s1, s2, subarray = submatrix(filename)
    F = wypelnijTablicePunktow(gap, seq1Data.seq, seq2Data.seq, s1, s2, subarray)
    sciezka, score, aln1, aln2, starts1, ends1, starts2, ends2 = getSciezkaDopasowania(seq1Data.seq, seq2Data.seq, F,
                                                                                       gap, s1, s2, subarray)
    import matplotlib.pyplot as plt
    import seaborn as sns

    yticks = split("-" + seq1Data.seq)
    xticks = split("-" + seq2Data.seq)
    plt.figure(figsize=(30, 30))

    ax = sns.heatmap(F, yticklabels=yticks, xticklabels=xticks, cbar=False, square=True)
    ax.tick_params(axis='both', which='major', labelsize=5)
    plt.xlabel(seq2Data.description, fontsize=30)
    plt.ylabel(seq1Data.description,fontsize=30)
    start = np.argwhere(sciezka == 1)
    for x in start:
        i = x[0]
        j = x[1]

        ax.add_patch(Rectangle((i, j), 1, 1, fill=False, edgecolor='blue', lw=3))

    ax.xaxis.set_ticks_position('top')
    
    fig = ax.get_figure()
    fig.savefig(input("Podaj nazwę pliku do zapisania graficznej tablicy punktacji:\n"))
    plt.show()
    f = open(input("Podaj nazwę pliku do zapisania statystyk"), "w+")
    alingment = ""
    for x in range(0, len(aln1)):
        if aln1[x] == aln2[x]:
            alingment = alingment + "|"
        else:
            alingment = alingment + " "

    f.write(
        '# 1: {}\n# 2: {}\n# Gap: {}\n# Score: {}\n{} {}>{}\n{}\n{} {}>{}'.format(
            seq1Data.seq, seq2Data.seq, gap, score, aln1, starts1, ends1, alingment, aln2, starts2, ends2))
    f.close()
